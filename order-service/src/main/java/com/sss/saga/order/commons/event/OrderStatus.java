package com.sss.saga.order.commons.event;

public enum OrderStatus {

    ORDER_CREATED,ORDER_COMPLETED,ORDER_CANCELLED
}
