package com.sss.saga.payment.commons.event;

public enum PaymentStatus {

    PAYMENT_COMPLETED,PAYMENT_FAILED
}
