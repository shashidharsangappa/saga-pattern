package com.sss.saga.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sss.saga.payment.entity.UserTransaction;

public interface UserTransactionRepository extends JpaRepository<UserTransaction,Integer> {
}
