package com.sss.saga.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sss.saga.payment.entity.UserBalance;

public interface UserBalanceRepository extends JpaRepository<UserBalance,Integer> {
}
