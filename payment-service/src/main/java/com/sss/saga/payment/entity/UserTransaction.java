package com.sss.saga.payment.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserTransaction {
	@Id
	private Integer orderId;
	private int userId;
	private int amount;

	public UserTransaction() {
		super();
	}

	public UserTransaction(Integer orderId, int userId, int amount) {
		super();
		this.orderId = orderId;
		this.userId = userId;
		this.amount = amount;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
